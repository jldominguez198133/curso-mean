'use strict';

var mongoose = require('mongoose');
var app = require('./app.js');
var port = process.env.PORT || 3678;

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/cursofavoritos', function(err, res) {
    if(err) {
        throw err;
    } else {
        console.log('Conexión a MongoDB correcta');
        app.listen(port, function () {
            console.log('API REST FAVORITOS funcionando en http://localhost:' + port);
        });
    }
});
