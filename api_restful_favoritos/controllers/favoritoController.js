'use strict';

var Favorito = require('../models/favoritoModels');

function prueba(req, res) {

    if(req.params.nombre) {
        var nombre = req.params.nombre;
    } else {
        var nombre = 'SIN NOMBRE';
    }


    res.status(200).send({
        message: 'Hola mundo con NodeJS y Express - ' + nombre
    });
}

function getFavorito(req, res) {
    var favoritoId = req.params.id;

    Favorito.findById(favoritoId, function(err, favorito) {
        if(err) {
            res.status(500).send({message: 'Error al devolver el marcador solicitado'});
        } else {
            if(!favorito) {
                res.status(404).send({message: 'No existe el marcador solicitado'});
            } else {
                res.status(200).send({marcador: favorito})
            }
        }
    });
}

function getFavoritos(req, res) {
    Favorito.find({}).sort('-_id').exec(function(err, favoritos) {
       if(err) {
           res.status(500).send({message: 'Error al devolver los marcadores'})
       } else {
           if(!favoritos) {
                res.status(404).send({message: 'No existen marcadores'});
           } else {
               res.status(200).send({listado: favoritos});
           }
       }
    });
}

function saveFavorito(req, res) {
    var favorito = new Favorito();
    var params = req.body;

    favorito.title = params.title;
    favorito.description = params.description;
    favorito.url = params.url;

    favorito.save(function(err, favoritoStored) {
        if(err) {
            res.status(500).send({message: 'Error al guardar el marcador'});
        } else {
            res.status(200).send({favorito: favoritoStored});
        }
    });
}

function updateFavorito(req, res) {
    var favoritoId = req.params.id;
    var update = req.body;

    Favorito.findByIdAndUpdate(favoritoId, update, function(err, favoritoUpdate) {
        if(err) {
            res.status(500).send({message: 'Error al actualizar el marcador'});
        } else {
            res.status(200).send({favorito: favoritoUpdate});
        }
    });
}

function deleteFavorito(req, res) {
    var favoritoId = req.params.id;

    Favorito.findById(favoritoId, function(err, favorito) {
        if(err) {
            res.status(500).send({message: 'Error al devolver el marcador solicitado'});
        } else {
            if(!favorito) {
                res.status(404).send({message: 'No existe el marcador solicitado'});
            } else {
                favorito.remove(function(err) {
                   if(err) {
                       res.status(500).send({message: 'Error al borrar el marcador'});
                   } else {
                       res.status(200).send({message: 'El marcador se ha eliminado'});
                   }
                });
            }
        }
    });
}

module.exports = {
    fnPrueba: prueba,
    fnGetFavorito: getFavorito,
    fnGetFavoritos: getFavoritos,
    fnSaveFavorito: saveFavorito,
    fnUpdateFavorito: updateFavorito,
    fnDeleteFavorito: deleteFavorito
};