'use strict';

var express = require('express');
var FavoritoController = require('../controllers/favoritoController');
var api = express.Router();

api.get('/prueba/:nombre?', FavoritoController.fnPrueba);
api.get('/favorito/:id', FavoritoController.fnGetFavorito);
api.get('/favoritos', FavoritoController.fnGetFavoritos);
api.post('/favorito', FavoritoController.fnSaveFavorito);
api.put('/favorito/:id', FavoritoController.fnUpdateFavorito);
api.delete('/favorito/:id', FavoritoController.fnDeleteFavorito);

module.exports = api;