'use strict';

let mongoose = require('mongoose'),
    app = require('./app'),
    port = process.env.PORT || 3700;

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/app_albums', { useMongoClient: true }, (err, res) => {
    if (err) {
        throw err;
    } else {
        console.log("Base de datos funcionando correctamente");

        app.listen(port, () => {
            console.log('API RESTful de albumes escuchando...');
        });
    }
});
