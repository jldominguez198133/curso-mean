'use strict';

let express = require('express');
let ImageController = require('../controllers/image');
let api = express.Router();

let multipart = require('connect-multiparty');
let multipartMiddleware = multipart({ uploadDir: './uploads/' });

api.get('/prueba-image', ImageController.pruebas); // Url de prueba
api.get('/image/:id', ImageController.getImage); // Obtiene una imagen
api.get('/images/:album?', ImageController.getImages); // Obtiene las imágenes de un album o todas las imágenes de todos los albumes
api.post('/image', ImageController.saveImage); // Guarda una imagen
api.put('/image/:id', ImageController.updateImage); // Edita una imagen
api.delete('/image/:id', ImageController.removeImage); // Borra una imagen
api.post('/upload-image/:id', multipartMiddleware, ImageController.uploadImage); // Subir una imagen - Hemos añadido un middleware con la ruta a donde deben subirse las imágenes
api.get('/get-image/:imageFile', multipartMiddleware, ImageController.getImageFile); // Obtiene una imagen concreta

module.exports = api;